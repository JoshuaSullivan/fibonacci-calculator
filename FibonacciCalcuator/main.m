//
//  main.m
//  FibonacciCalcuator
//
//  Created by Joshua Sullivan on 5/6/15.
//  Copyright (c) 2015 The Nerdery. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
