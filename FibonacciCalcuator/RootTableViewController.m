//
//  RootTableViewController.m
//  FibonacciCalcuator
//
//  Created by Joshua Sullivan on 5/6/15.
//  Copyright (c) 2015 The Nerdery. All rights reserved.
//

#import "RootTableViewController.h"

static NSString * const kCellReuseIdentifier = @"kCellReuseIdentifier";
static NSString * const kSummaryCellIdentifier = @"kSummaryCellIdentifier";
static const NSUInteger kIterationsToCalculate = 301;

@interface RootTableViewController ()

@property (strong, nonatomic) NSMutableArray *results;
@property (strong, nonatomic) NSString *summary;
@property (assign, nonatomic) BOOL isCalculating;

@end

@implementation RootTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.results = [NSMutableArray arrayWithCapacity:kIterationsToCalculate];
    self.results[0] = @"0";
    self.results[1] = @"1";

    self.isCalculating = YES;
    NSTimeInterval startTime = CACurrentMediaTime();
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *answer = [self calculateFibonocciToIteration:kIterationsToCalculate];
        NSLog(@"[n=%lu] %@", (unsigned long)kIterationsToCalculate, answer);
        NSTimeInterval endTime = CACurrentMediaTime();
        dispatch_async(dispatch_get_main_queue(), ^{
            self.summary = [NSString stringWithFormat:@"Calculated %lu iterations in %0.2fms.",(unsigned long)kIterationsToCalculate, (endTime - startTime) * 1000.0];
            self.isCalculating = NO;
            [self.tableView reloadData];
        });
    });

}

/**
* Checks the previously computed values and returns one if it has already been computed (yay, memoization!).
* Otherwise, continuously performs computation (storing intermediate results) to achieve the answer.
*/
- (NSString *)calculateFibonocciToIteration:(NSUInteger)iteration
{
    if (iteration < self.results.count) {
        return self.results[iteration];
    }
    NSUInteger remainingIterations = iteration - self.results.count;
    do {
        NSUInteger count = self.results.count;
        [self.results addObject:[self addString:self.results[count - 1] toSecondString:self.results[count - 2]]];
        remainingIterations--;
    } while (remainingIterations > 0);
    return [self.results lastObject];
}

/**
* Since none of the standard library numeric types are big enough (UInt64) or accurate enough (double),
* we will do synthetic string addition instead. This is dramatically slower than numeric math, but calculating the
* Fibonacci sequence out to several hundred iterations still requires very little time.
*/
- (NSString *)addString:(NSString *)firstString toSecondString:(NSString *)secondString
{
    static NSCharacterSet *_nonNumericSet;
    if (!_nonNumericSet) {
        _nonNumericSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    }
    if ([firstString rangeOfCharacterFromSet:_nonNumericSet].location != NSNotFound) {
        NSAssert(NO, @"ERROR: firstString must contain only numeric digits.");
        return nil;
    }
    if ([secondString rangeOfCharacterFromSet:_nonNumericSet].location != NSNotFound) {
        NSAssert(NO, @"ERROR: secondString must contain only numeric digits.");
        return nil;
    }
    NSInteger len0 = firstString.length;
    NSInteger len1 = secondString.length;
    NSInteger maxLen = MAX(len0, len1);
    NSInteger offset = 1;
    NSInteger carryOver = 0;
    NSString *result = @"";
    do {
        NSInteger index0 = len0 - offset ;
        NSInteger int0 = (index0 >= 0) ? [[firstString substringWithRange:NSMakeRange((NSUInteger)index0, 1)] integerValue] : 0;
        NSInteger index1 = len1 - offset;
        NSInteger int1 = (index1 >= 0) ? [[secondString substringWithRange:NSMakeRange((NSUInteger)index1, 1)] integerValue] : 0;
        NSInteger sum = int0 + int1 + carryOver;
        NSInteger columnResult = sum % 10;
        result = [[NSString stringWithFormat:@"%li", (long)columnResult] stringByAppendingString:result];
        carryOver = sum / 10;
        offset++;
    } while (offset <= maxLen);
    if (carryOver > 0) {
        result = [[NSString stringWithFormat:@"%li", (long)carryOver] stringByAppendingString:result];
    }
    return result;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isCalculating) {
        return 1;
    }
    return self.results.count + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isCalculating) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSummaryCellIdentifier forIndexPath:indexPath];
        cell.textLabel.text = @"Calculating…";
        return cell;
    }
    if (indexPath.row < kIterationsToCalculate) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifier forIndexPath:indexPath];

        cell.textLabel.text = [NSString stringWithFormat:@"%li", (long)indexPath.row];
        cell.detailTextLabel.text = self.results[(NSUInteger)indexPath.row];

        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSummaryCellIdentifier forIndexPath:indexPath];
        cell.textLabel.text = self.summary;
        return cell;
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
