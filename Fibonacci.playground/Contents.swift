import UIKit

/** This character set lets us strictly enforce our requirement that strings contain only numeric characters. */
let nonNumericSet = NSCharacterSet(charactersInString: "1234567890").invertedSet

func addStrings(a : String, b : String) -> String
{
    // Cast the Strings to NSStrings so that we can do indexed access of them.
    var aStr = NSString(string: a)
    var bStr = NSString(string: b)
    
    // Verify that both strings only contain numeric characters.
    if(aStr.rangeOfCharacterFromSet(nonNumericSet).location != NSNotFound) {
        assert(false, "String 'a' must only contain numeric characters")
        return "NaN"
    }
    if (bStr.rangeOfCharacterFromSet(nonNumericSet).location != NSNotFound) {
        assert(false, "String 'a' must only contain numeric characters")
        return "NaN"
    }
    
    // Set up the result accumulator string and the indicies.
    var result = ""
    var aIndex = aStr.length
    var bIndex = bStr.length
    
    // Overflow needs to be tracked between iterations of the loop.
    var overflow = 0
    do {
        // If either of the strings still have a positive index, use a character from the string, zero otherwise.
        let aInt = (aIndex > 0) ? aStr.substringWithRange(NSMakeRange(aIndex - 1, 1)).toInt()! : 0
        let bInt = (bIndex > 0) ? bStr.substringWithRange(NSMakeRange(bIndex - 1, 1)).toInt()! : 0
        let sum = aInt + bInt + overflow
        
        // Add the ones position of the sum to the result
        result += String(sum % 10)
        
        // Carry the remainter to the next calculation
        overflow = sum / 10
        
        // Decriment the indices
        aIndex--
        bIndex--
    } while (aIndex > 0 || bIndex > 0) // Once both indices are 0 or less, we're done.
    
    // If we have any carryover left, add it to the accumulator.
    if (overflow > 0) {
        result += String(overflow)
    }
    // Since we accumulate the characters in reverse order, we need to reverse our final result.
    return String(reverse(result))
}

/** This stores the calculated iterations of the Fibonacci sequence, allowing memoization. */
var results = ["0", "1"]

/** Calculates the Fibonacci sequence for the specified number of iterations. */
func fibonacciSequential(iterations: Int) -> String
{
    let storedCount = count(results)
    
    // If we already have the result for this number of iterations, simply return it.
    if (iterations < storedCount) {
        return results[iterations]
    }
    
    // Start the cacluations using the largest stored results. */
    var index = storedCount
    do {
        // Calculate the value and store it in the results array.
        results += [addStrings(results[index - 1], results[index - 2])]
        index++
    } while (index <= iterations)
    // Return the final calculation result.
    return results.last!
}

let fib100 = fibonacciSequential(100)
let fib50 = fibonacciSequential(50)

